var cow;

Cow = function() {
    this.sound = "Moooh";
};

Cow.prototype.makeSound = function() {
    console.log(this.sound)
};

Cow.prototype.makeSoundDelayedWithThis = function(delay) {

    setTimeout(
        function() {
            console.log("Call to cow.makeSoundDelayedWithThis(" + delay +")");
            try {
                this.makeSound()
            } catch (error) {
                console.error("cannot call this.makeSound(): " + error);
            }
        },
        delay
    );
};

Cow.prototype.makeSoundDelayedWithThat = function(delay) {
    var that = this;
    setTimeout(
        function() {
            console.log("Call to cow.makeSoundDelayedWithThat(" + delay +")");
            that.makeSound()
        },
        delay
    );
};

cow = new Cow();
console.log("Call to cow.makeSound()");
cow.makeSound();
cow.makeSoundDelayedWithThis(1000);
cow.makeSoundDelayedWithThat(2000);

